# README

Code written in Python to extract retailers' NAICS from Industry Canada.

## Requirements
The script uses both `Python` and `Scrapy`. Familiarity with both is somewhat assumed.

## Resources
For Rallye's benefit, here are some resources to learn `Scrapy`, assuming you are comfortable with `Python`:

- [Scrapy](http://doc.scrapy.org/en/latest/intro/tutorial.html): Official docs (warning: Scrapy has a steep learning curve, but well worth it if you're willing to invest the time)
- [Scraping Web Pages with Scrapy](http://youtu.be/1EFnX1UkXVU): Youtube video (Herman has a series of excellent video tutorials on `Scrapy`)
- [Scrapy: it GETs the web](http://youtu.be/-JzH8TcwqxI): Another Youtube video. There's a pattern here.
- [Recursively Scraping Web Pages with Scrapy](http://youtu.be/P-_TpZ54Vcw): Yet another Youtube video... there's a pattern here.
- [Scraping the Web with Scrapy](http://youtu.be/eD8XVXLlUTE): Sitll Youtube. It's a little more technical.