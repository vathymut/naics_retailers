# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class NaicsRetailersItem(Item):
    # define the fields for your item here like:
    legal_name = Field()
    op_name = Field()
    naics = Field()
    naics_alt = Field()
