# Scrapy settings for naics_retailers project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'naics_retailers'

SPIDER_MODULES = ['naics_retailers.spiders']
NEWSPIDER_MODULE = 'naics_retailers.spiders'
DOWNLOAD_DELAY = 0.25    # 250 ms of delay

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'naics_retailers (+http://www.yourdomain.com)'
