# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        naics_retail.py
# Purpose:     To extract retailers' NAICS from Industry Canada.
#
# Author:      Vathy M. Kamulete
#
# Created:     07/04/2014
# Copyright:
# Licence:     GNU GPL
#-------------------------------------------------------------------------------

from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.spider import BaseSpider
from naics_retailers.items import NaicsRetailersItem
from unidecode import unidecode
from time import sleep

# Scraper starts from this webpage
STARTURL = "http://www.ic.gc.ca/app/ccc/sld/cmpny.do?lang=eng&profileId=1921&naics=44-45"

# Scraper follows links relative to the URLSTUMP
URLSTUMP = "http://www.ic.gc.ca/app/ccc/sld/"

# Scraper follows links relative to the RETAILERSTUMP
RETAILERSTUMP = "http://www.ic.gc.ca"

# XPATH Selectors to follow links and extract information
LEGAL_NAME_XPATH = "//tr/td[contains(., 'Legal Name')]/text()"
OP_NAME_XPATH = "//tr/td[contains(., 'Operating Name')]/text()"
NAICS_XPATH = "//tr/td[contains(., 'Primary Industry')]/following-sibling::*/text()"
NAICS_ALT_XPATH = "//tr/td[contains(., 'Alternate Industries')]/following-sibling::*/text()"
ALPHA_LIST_XPATH = "//ul[@class='noBullet alphaPicklist']/li/a/@href"
COMPANY_XPATH = "//li[@class='companyLinks']/a[contains(@title, 'Complete profile')]/@href"

# Function to gather final info
def gather_info( hxs ):
    '''
    Uses XPATH Selectors defined above to gather:
        op_name - Operating name
        legal_name - Legal name
        naics - Primary NAICS Code
        naics - Alternative NAICS Code
    Return a Scrapy item object to be saved in a dataset.
    hxs is the HtmlXPathSelector of the response.
    '''
    legal_name = hxs.select( LEGAL_NAME_XPATH ).extract()
    op_name = hxs.select( OP_NAME_XPATH ).extract()
    naics = hxs.select( NAICS_XPATH ).extract()
    naics_alt = hxs.select( NAICS_ALT_XPATH ).extract()
    # Strip whitespaces and convert unicode to ascii
    legal_name = [ unidecode( s.strip() ) for s in legal_name ]
    op_name = [ unidecode( s.strip() ) for s in op_name ]
    naics = [ unidecode( s.strip() ) for s in naics ]
    naics_alt = [ unidecode( s.strip() )for s in naics_alt ]
    # Load the dictionary where to store the information
    item = NaicsRetailersItem()
    item[ 'op_name' ] = op_name
    item[ 'legal_name' ] = legal_name
    item[ 'naics' ] = naics
    item[ 'naics_alt' ] = naics_alt
    return item

# REMINDER:
# COMMENT CODE EXTENSIVELY TO HELP RALLYE DECIPHER IT
class RetailersNaics(BaseSpider):
    '''
    Spider to extract retailers' NAICS from Industry Canada.
    '''
    name = "naics"
    allowed_domains = ["ic.gc.ca"]
    start_urls = [ STARTURL ]

    def parse(self, response):
        """
		Get links to alphabetical listing of retailers' info.
        """
        # Parse webpage as XPATH
        hxs = HtmlXPathSelector(response)
        # Extract all relevant links using XPATH query
        links = hxs.select( ALPHA_LIST_XPATH ).extract()
        # Save links to file
        with open( 'all_links.txt', 'a+') as f:
            for link in links:
                f.write( URLSTUMP + link + '\n' )
        for link in links:
            # Follow links from alphabetical listing
            # Once the scraper gets the website, it applies
            # the method 'get_retailers' to it
            yield Request( URLSTUMP + link, callback = self.get_retailers )

    def get_retailers(self, response):
        """
		Get links to retailers' complete profile page.
		"""
        # Parse webpage as XPATH
        hxs = HtmlXPathSelector( response )
        # Extract all relevant links using XPATH query
        links = hxs.select( COMPANY_XPATH ).extract()
        # Save links to file
        with open( 'retailers_links.txt', 'a+' ) as f:
            for link in links:
                f.write( link + '\n' )
        for link in links:
            yield Request( RETAILERSTUMP + link, callback = self.extract_info )

    def extract_info( self, response ):
        """
        Extract info from the retailer's profile page.
        """
        self.log( 'A response from %s just arrived!' % response.url )
        hxs = HtmlXPathSelector( response )
        # Gather the information from the XPATH given
        item = gather_info( hxs )
        # Time out between requests to website to not overload the server
        sleep( 1.0 )
        yield item
        # Record successful attempts
        with open( 'retailers_parsed.txt', 'a+' ) as f:
            f.write( response.url + '\n' )
